function _(id){
    return document.getElementById(id);
}

let btnQuestions, additionalQuestions, backToTop, preloader, btnMenu, btnX, btnSubmit, inputName, inputMail, textarea;
btnQuestions = _("btn-questions");
additionalQuestions = _("additional-questions");
backToTop = _("back-to-top");
preloader = _("preloader");
btnMenu = _("btnMenu");
btnX = _("btnX");
btnSubmit = _("submit");
inputName = _("input-name");
inputMail = _("input-mail");
textarea = _("textarea");




// Click to display additional questions
btnQuestions.addEventListener('click', () => {
    btnQuestions.style.display = "none";
    additionalQuestions.classList.add("active");
});

// "Scroll top" button
backToTop.addEventListener('click', () => {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
});

// Responsive menu animations
btnMenu.addEventListener('click', () => {
    document.body.classList.toggle('menu-open');
    btnX.classList.toggle('is-active');
});

// Preloader animations
window.addEventListener('load', () => {
    setTimeout( () => {preloader.classList.add('preloader-hidden')}, 500);
    setTimeout( () => {preloader.classList.add('preloader-none')}, 1000);
});

// Form validation and console logging
btnSubmit.addEventListener('click', (e) => {
    e.preventDefault();
    let valid = true;
    let name = inputName.value;
    let mail = inputMail.value;
    let phone = _("input-phone").value;
    let company = _("input-company").value;
    let url = _("input-url").value;
    let textarea = _("textarea").value;
    let timeframe = _("timeframe").value;
    let budget = _("budget").value;
    let google = _("option-google").checked;
    let bing = _("option-bing").checked;
    let facebook = _("option-facebook").checked;
    let linkedin = _("option-linkedin").checked;
    let magazine = _("option-magazine").checked;
    let radio = _("option-radio").checked;
    let recommendation = _("option-recommendation").checked;
    let other = _("option-other").checked;

    if( name == "" || !isNameValid(name)){
        valid = false;
        document.querySelector(".name-error").style.display = "block";
    } else {
        document.querySelector(".name-error").style.display = "none";
    }
    if ( mail == "" || !isEmailValid(mail)){
        valid = false;
        document.querySelector(".email-error").style.display = "block";
    } else {
        document.querySelector(".email-error").style.display = "none";
    }
    if ( textarea == ""){
        valid = false;
        document.querySelector(".field-error").style.display = "block";
    } else {
        document.querySelector(".field-error").style.display = "none";
    }
    if(valid == true){
        var form_data = {
            name: name,
            email: mail,
            phone: phone,
            company: company,
            url: url,
            text: textarea,
            timeframe: timeframe,
            budget: budget,
            google: google,
            bing: bing,
            facebook: facebook,
            linkedin: linkedin,
            magazine: magazine,
            radio: radio,
            recommendation: recommendation,
            other: other
    }
    console.table(form_data);
};
})

// Validators
function isNameValid(name){
    return /[A-Z]+/.test(name) && /[a-z]+/.test(name) && /\S{2,15}/.test(name);
}
function isEmailValid(name){
    var pattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    return pattern.test(name)
}



// Upload image
_("upload-image").addEventListener("click", () => {
    document.querySelector(".task-boxes").style.display = "flex";
});
function showImage(){
    if(this.files && this.files[0])
    {
        var obj = new FileReader();
        obj.onload = function(data){
            var image = _("image");
            image.src = data.target.result;
            image.style.display = "block";
        }
        obj.readAsDataURL(this.files[0]);
    }
}